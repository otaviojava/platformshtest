package com.platformshtest.mvc.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.platformshtest.mvc.model.Person;

@RestController
public class PersonController {

	@RequestMapping("/rest")
	public String healthCheck() {
		return "OK";
	}
	
	@RequestMapping("/person")
	public Person getPerson(@RequestParam(name="name") String name) {
		Person person = new Person();
		person.setName(name);
		return person;
	}
}
